package com.ykl.tm.controller;

import com.ykl.tm.util.SSOClientUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {
  @RequestMapping("/tm")
  public String tm(Model model) {
    model.addAttribute("logOutUrl", SSOClientUtil.getServerLogOutUrl());
    return "tm";
  }
  @RequestMapping("/logOut")
  public void logout(HttpSession session, HttpServletResponse response) {
    session.invalidate();
  }
}
