package com.example.demo.db;

import com.example.demo.pojo.ClientInfoVo;

import java.util.*;

public class MockDB {
  public static Set<String> T_TOKEN = new HashSet<String>();
  public static Map<String, List<ClientInfoVo>> T_CLIENT_INFO = new HashMap<String, List<ClientInfoVo>>();
}
