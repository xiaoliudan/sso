package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication

@ServletComponentScan  //扫描注册监听器
public class SsoServerApplication {

  public static void main(String[] args) {
    SpringApplication.run(SsoServerApplication.class, args);
  }

}
