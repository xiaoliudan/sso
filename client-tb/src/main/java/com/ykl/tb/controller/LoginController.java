package com.ykl.tb.controller;

import com.ykl.tb.util.SSOClientUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {
  @RequestMapping("/tb")
  public String tb(Model model) {
    model.addAttribute("logOutUrl", SSOClientUtil.getServerLogOutUrl());
    return "tb";
  }
  @RequestMapping("/logOut")
  public void logout(HttpSession session,HttpServletResponse response) {
    session.invalidate();
  }
}
